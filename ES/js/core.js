var menuOpen = function () {
  $("#menu").addClass("active")
},
  menuClose = function () {
    $("#menu").removeClass("active")
  };
$("a.scroll").bind("click", function (e) {
  menuClose();
});
$("#lang-wrapper .active").bind("click", function (e) {
  e.preventDefault()
});
$(window).on('load', function () {
  $('iframe').each(function(){
    $(this).attr('src', $(this).attr('data-src'));
  });
  $('.lazyload').each(function(){
    $(this).attr('src', $(this).attr('data-src'));
    $(this).attr('srcset', $(this).attr('data-srcset'));
  });
  $('#video iframe').attr('src', $('#video iframe').attr('data-src'));
  $('#shop-now-cta').on('click', function (e) {
    e.preventDefault();
    //tURL = $('.owl-item.active').find('img').attr('data-href');
    tURL = 'https://www.farmasius.com/category/vfx-elite/1636'
    $(this).attr('href', tURL);
    //console.log($(this).attr('href'));
    window.location.href = tURL;
  });
  $('.jarallax').jarallax();
});
$.fn.isInViewport = function () {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};